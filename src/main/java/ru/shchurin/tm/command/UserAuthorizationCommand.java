package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserAuthorizationCommand extends AbstractCommand {
    private final boolean safe = true;
    private final ArrayList<Role> roles = new ArrayList<>();

    @Override
    public String getCommand() {
        return "user-authorization";
    }

    @Override
    public String getDescription() {
        return "Authorise user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER AUTHORIZATION]");
        System.out.println("ENTER LOGIN:");
        final String login = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER PASSWORD:");
        final String password = ConsoleUtil.getStringFromConsole();
        final String hashOfPassword = serviceLocator.getUserService().getHashOfPassword(password);
        final User currentUser = serviceLocator.getUserService().authoriseUser(login, hashOfPassword);
        if (currentUser == null) {
            System.out.println("[USER NOT FOUND]");
        } else {
            serviceLocator.setCurrentUser(currentUser);
            System.out.println("[USER AUTHORIZED]");
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
