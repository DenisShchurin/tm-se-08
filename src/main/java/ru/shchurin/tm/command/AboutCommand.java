package ru.shchurin.tm.command;

import com.jcabi.manifests.Manifests;
import ru.shchurin.tm.entity.Role;

import java.util.ArrayList;
import java.util.List;

public class AboutCommand extends AbstractCommand{
    private final boolean safe = true;
    private final ArrayList<Role> roles = new ArrayList<>();

    @Override
    public String getCommand() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Application assembly information.";
    }

    @Override
    public void execute() {
        System.out.println("Manifest-Version: " + Manifests.read("Manifest-Version"));
        System.out.println("Built-By: " + Manifests.read("Built-By"));
        System.out.println("Created-By: " + Manifests.read("Created-By"));
//        System.out.println(Manifests.read("Foo-Version"));
//        System.out.println(Manifests.read("Project-Name"));
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
