package ru.shchurin.tm;

import ru.shchurin.tm.bootstrap.Bootstrap;

public final class Application {
    public static void main( String[] args ) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}
