package ru.shchurin.tm.exception;

public class ConsoleHashPasswordException extends Exception {
    public ConsoleHashPasswordException() {
        super("YOU ENTERED INCORRECT PASSWORD");
    }
}
