package ru.shchurin.tm.bootstrap;

import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.service.ProjectService;
import ru.shchurin.tm.service.ProjectTaskService;
import ru.shchurin.tm.service.TaskService;
import ru.shchurin.tm.service.UserService;

import java.util.List;

public interface ServiceLocator {
    List<AbstractCommand> getCommands();

    ProjectService getProjectService();

    TaskService getTaskService();

    ProjectTaskService getProjectTaskService();

    UserService getUserService();

    User getCurrentUser();

    void setCurrentUser(final User currentUser);
}
