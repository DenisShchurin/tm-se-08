package ru.shchurin.tm.entity;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN;

    public String displayName() {
        return name();
    }
}
