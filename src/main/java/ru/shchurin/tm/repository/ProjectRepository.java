package ru.shchurin.tm.repository;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.exception.AlreadyExistsException;

import java.util.List;

public interface ProjectRepository {
    List<Project> findAll(String userId);

    Project findOne(String userId, String id);

    void persist(Project project) throws AlreadyExistsException;

    void merge(Project project);

    void remove(String userId, String id);

    void removeAll(String userId);

    void removeByName(String userId, String name);
}
