package ru.shchurin.tm.repository;

import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.AlreadyExistsException;
import ru.shchurin.tm.exception.ConsoleHashPasswordException;
import ru.shchurin.tm.exception.ConsoleLoginException;

import java.util.Collection;

public interface UserRepository {
    Collection<User> findAll();

    User findOne(String id);

    void persist(User user) throws AlreadyExistsException, ConsoleLoginException, ConsoleHashPasswordException;

    void merge(User user);

    void remove(String id);

    void removeAll();

    void removeByLogin(String login);

    boolean updatePassword(String login, String hashPassword, String newHashPassword);
}
