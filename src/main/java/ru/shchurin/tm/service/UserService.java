package ru.shchurin.tm.service;

import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.AlreadyExistsException;
import ru.shchurin.tm.exception.ConsoleHashPasswordException;
import ru.shchurin.tm.exception.ConsoleLoginException;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;

public interface UserService {
    Collection<User> findAll();

    User findOne(String id) throws Exception;

    void persist(User user) throws AlreadyExistsException, ConsoleLoginException, ConsoleHashPasswordException;

    void merge(User user) throws Exception;

    void remove(String id) throws Exception;

    void removeAll();

    void removeByLogin(String login) throws Exception;

    String getHashOfPassword(String password) throws NoSuchAlgorithmException;

    User authoriseUser(String login, String hashOfPassword) throws Exception;

    boolean updatePassword(String login, String hashPassword, String newHashPassword);
}
