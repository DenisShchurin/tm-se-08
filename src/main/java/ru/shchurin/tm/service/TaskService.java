package ru.shchurin.tm.service;

import ru.shchurin.tm.entity.Task;

import java.util.List;

public interface TaskService {
    List<Task> findAll(String userId);

    Task findOne(String userId, String id) throws Exception;

    void persist(Task task) throws Exception;

    void merge(Task task) throws Exception;

    void remove(String userId, String id) throws Exception;

    void removeAll(String userId);

    void removeByName(String userId, String name) throws Exception;
}
